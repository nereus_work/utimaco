FROM debian:stretch
LABEL version="4.30" \
	description="Utimaco CryptoServer Simulator" \
	maintainer="frank.thunig@utimaco.com"

ARG SimulatorPort=3001

RUN dpkg --add-architecture i386; \
	apt-get update; \
	apt-get install -y \
		libc6-i386 \
		lib32gcc1 \
		lib32stdc++6; \
	rm -rf /var/lib/apt/lists/*

WORKDIR /simulator/bin
COPY sim5_linux /simulator

RUN chmod u+x ./bl_sim5

ENV SDK_PORT=$SimulatorPort
EXPOSE $SimulatorPort

STOPSIGNAL SIGINT
ENTRYPOINT ["./bl_sim5"]
CMD ["-o","-h"]