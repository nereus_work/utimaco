#!/bin/bash

if [ -z "$SDK_PORT" ]
then
  SDK_PORT=3001
fi

SDK_START_PORT=$SDK_PORT

SDK_BIN_PATH="$(dirname "$(readlink -f "$0")")"

SDK_INSTANCE_PATH=".tmp"


if [ ! -e $SDK_INSTANCE_PATH ]
then
  mkdir $SDK_INSTANCE_PATH
  i=1
  while [ $i -le $1 ]
  do
    mkdir -p "$SDK_INSTANCE_PATH/$i/bin" 
    cp "$SDK_BIN_PATH/bl_sim5"  "$SDK_INSTANCE_PATH/$i/bin/bl_sim_instance" 
    cp "$SDK_BIN_PATH/cs_sim.ini" "$SDK_INSTANCE_PATH/$i/bin" 
    cp $SDK_BIN_PATH/*.so $SDK_INSTANCE_PATH/$i/bin 2> /dev/null
    mkdir -p "$SDK_INSTANCE_PATH/$i/devices" 
    cp  -r "$SDK_BIN_PATH/../devices"  "$SDK_INSTANCE_PATH/$i" 

    xterm -T "Utimaco Cryptoserver SDK5 - Simulator $SDK_PORT@localhost" \
      -e "export SDK_PORT=$SDK_PORT; $SDK_INSTANCE_PATH/$i/bin/bl_sim_instance -h -o" &       
    pid=`ps -ax | grep $SDK_PORT@localhost | grep -v grep | cut -d" " -f2`
    i=`expr $i + 1`
    SDK_PORT=`expr $SDK_PORT + 2`
  done
else
   echo ".tmp directory already exists"
   echo "delete .tmp directory and start again"
   exit 0
fi

trap "rm -r $SDK_INSTANCE_PATH
      exit 0" SIGINT SIGTERM

echo 
echo "cs_multi: $1 instances started"
echo
read -n1 -r -p "Press any key to terminate and remove all instances" 
echo

pkill -SIGINT bl_sim_instance
 
rm -r $SDK_INSTANCE_PATH



